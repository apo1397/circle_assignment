name := """circle_assignment_v1"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  guice,
  "org.mongodb" % "mongo-java-driver" %"3.10.2",
  "org.jongo"%"jongo"%"1.3.1")
