THIS ASSIGNMENT IS FOR CIRCLE NEWS BY APOORV ABHISHEK

The requirement was to have a table with Users and a table with Blogs.
The application is using Java and its Play Framework and SBT is used to build it.
MongoDB is used as our Databse and Jongo is used as the ORM.

Requirements : 
Java 8
SBT

To run the application, please install sbt 
After installation go to directory and type "sbt run" in the command prompt

The schema of the models are as follows : 
Users : {
	"user_id":NumberLong,
	"user_name":String,
	"added_on":Date,
	"friends":List<NumberLong>
	}
Blog : {
	"content":String,
	"posted_by":NumberLong,
	"posted_on":Date,
	"comments":[{
			"comment":String,
			"posted_by":NumberLong,
			"posted_on":Date
			}]
	}

SAMPLE DATA FOR USERS :
{
	"_id" : ObjectId("5e1220deeb43e4ddc8d257d7"),
	"user_id" : NumberLong(1001),
	"user_name" : "Apoorv",
	"added_on" : ISODate("2020-01-05T17:46:06.268Z"),
	"friends" : [
		NumberLong(1002),
		NumberLong(1004)
	]
}
{
	"_id" : ObjectId("5e122139eb43e4ddc8d257d8"),
	"user_id" : NumberLong(1002),
	"user_name" : "Abhishek",
	"added_on" : ISODate("2020-01-05T17:47:37.843Z"),
	"friends" : [
		NumberLong(1001),
		NumberLong(1003),
		NumberLong(1004)
	]
}
{
	"_id" : ObjectId("5e122151eb43e4ddc8d257d9"),
	"user_id" : NumberLong(1003),
	"user_name" : "Amit",
	"added_on" : ISODate("2020-01-05T17:48:01.882Z"),
	"friends" : [
		NumberLong(1002),
		NumberLong(1005)
	]
}
{
	"_id" : ObjectId("5e12216ceb43e4ddc8d257da"),
	"user_id" : NumberLong(1004),
	"user_name" : "Vikram",
	"added_on" : ISODate("2020-01-05T17:48:28.849Z"),
	"friends" : [
		NumberLong(1001),
		NumberLong(1002),
		NumberLong(1005)
	]
}
{
	"_id" : ObjectId("5e13752824032406107e0d7c"),
	"user_id" : NumberLong(1005),
	"user_name" : "Karan",
	"added_on" : ISODate("2020-01-06T17:58:00.956Z"),
	"friends" : [
		NumberLong(1003),
		NumberLong(1004),
		NumberLong(1006)
	]
}



The Requirement was to create an API of the structure :
GET     /users/:userId/level/:levelNo  --- Its response will be a list of the Kth Level friends of the User where k = levelNo

The file mongo.conf shows that the db name is "circleAssignment" and the file MongoRepository.java shows that there are 2 collections, "users" and "blogs"

If you face any issues please contact me via mail ( apoorvsinha1397@gmail.com) or phone (8297019465)
