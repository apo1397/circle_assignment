package handler;

import com.google.inject.Inject;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.typesafe.config.Config;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

public class MongoConnectionHandler {
    private final Config config;
    private final MongoClient mongoClient;
    private final DB db;
    private Jongo jongo;

    @Inject
    public MongoConnectionHandler(Config config) {
        this.config = config;
        String host = this.config.getString("mongo.url");
        mongoClient = new MongoClient(host);
        db = mongoClient.getDB(this.config.getString("mongo.db"));
        jongo = new Jongo(db);
    }

    public MongoCollection getCollection(String collectionName){
        MongoCollection mongoCollection = null;
        try{
            mongoCollection = jongo.getCollection(collectionName);
        }catch (Exception e){
            throw e;
        }
        return  mongoCollection;
    }


}
