package repository;

import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import handler.MongoConnectionHandler;
import models.Users;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;

import java.util.ArrayList;
import java.util.List;

public class MongoRepository {
    private final MongoCollection userCollection;
    private final MongoCollection blogCollection;

    @Inject
    public MongoRepository(MongoConnectionHandler mongoConnectionHandler) {
        userCollection = mongoConnectionHandler.getCollection("users");
        blogCollection = mongoConnectionHandler.getCollection("blogs");
    }

    public List<Long> getFriendList(Long userId){
        BasicDBObject query = new BasicDBObject();
        query.append("user_id",userId);
        List<Long> userIds = new ArrayList<>();
        Users user = new Users();
        try{
            user = userCollection.findOne(query.toString()).as(Users.class);
        }catch (Exception e){
            throw e;
        }
        return user.getFriends();
    }
}
