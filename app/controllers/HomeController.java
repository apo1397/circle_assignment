package controllers;

import com.google.inject.Inject;
import play.libs.Json;
import play.mvc.*;
import services.HomeService;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
    private final HomeService homeService;
    private static final String INVALID_REQUEST = "INVALID REQUEST";

    @Inject
    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }

    public Result getKthFriends(Long userId, Integer levelNo) throws Exception {
        if(null == userId || null == levelNo){
            throw new Exception(INVALID_REQUEST);
        }
        List<Long> friends = null;
        try{
           friends = homeService.getKthFriendsofUser(userId,levelNo);
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
       return ok(Json.toJson(friends));
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }
    
    public Result explore() {
        return ok(views.html.explore.render());
    }
    
    public Result tutorial() {
        return ok(views.html.tutorial.render());
    }

}
