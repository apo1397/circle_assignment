package services;

import com.google.inject.Inject;
import models.Users;
import repository.MongoRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HomeService {
    private final MongoRepository mongoRepository;

    @Inject
    public HomeService(MongoRepository mongoRepository) {
        this.mongoRepository = mongoRepository;
    }

    public List<Long> getKthFriendsofUser(Long userId, Integer level) throws Exception {
        List<Long> levelUsers = mongoRepository.getFriendList(userId); //get List of 1st level Friends for user
        Set<Long> visited = new HashSet<>(); //unique list of visited friends
        visited.add(userId);
        if (levelUsers.size() > 0) {
            while (level > 1) {
                List<Long> currentLevelUsers = new ArrayList<>(); //keep current friends at each level
                for (Long id : levelUsers) {
                    if (!visited.contains(id)) {
                        visited.add(id);
                        currentLevelUsers.addAll(mongoRepository.getFriendList(id));
                    }
                }
                levelUsers = new ArrayList<>();
                for (Long friend : currentLevelUsers) {
                    if (!visited.contains(friend)) levelUsers.add(friend); //if never visted then add friend
                }
                level--;
            }
        }
        else{
            throw new Exception("USER HAS NO FRIENDS");
        }
        return levelUsers;
    }
}
