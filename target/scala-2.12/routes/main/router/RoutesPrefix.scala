// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/amitvikram/Downloads/circle_assignment_v1/conf/routes
// @DATE:Sun Jan 05 22:55:27 IST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
